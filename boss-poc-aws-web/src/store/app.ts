import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators'
import ApiService from '../services/api.service'

@Module({
  name: 'app',
  stateFactory: true,
  namespaced: true
})
export default class AppModule extends VuexModule {
  metrics: Array<string> = []

  metricDataResults: Array<{
    timestamp: string,
    value: string
  }> = []

  @Mutation
  setMetrics (metrics: Array<string>) {
    this.metrics = metrics
  }

  @Action
  async loadMetrics () {
    const metrics = await ApiService.get<Array<string>>('cloudwatch/metric/list', { 'namespace': 'EC2' })
    this.setMetrics(metrics.data)
  }

  @Mutation
  setMetricDataResults (metricDataResults: Array<{
    timestamp: string,
    value: string
  }>) {
    this.metricDataResults = metricDataResults
  }

  @Action
  async loadMetricDataResults(metricName: string) {
    const metricDataResults = await ApiService.get<Array<{
    timestamp: string,
    value: string
  }>>('cloudwatch/metric/data', { 'namespace': 'EC2', 'metricName': metricName, 'stat': 'Average' })
    this.setMetricDataResults(metricDataResults.data)
  }
}
