import axios, {AxiosRequestConfig, AxiosResponse} from 'axios';
import Vue from "vue";
import VueAxios from "vue-axios";

const API_URL = process.env.VUE_APP_API_URL

const ApiService = {
  init() {
    Vue.use(VueAxios, axios);
    Vue.axios.defaults.baseURL = API_URL;
  },
  post<R>(apiPath: string, request: any): Promise<AxiosResponse<R>> {
    return Vue.axios.post<R>(apiPath, request);
  },
  postFormData<R>(apiPath: string, request: any, onUploadProgress: Function = (percentage: number): void => {}): Promise<AxiosResponse<R>> {
    let formData = new FormData();
    Object.entries(request).forEach(entry => {
      const value: string|Blob = entry[1] as string|Blob
      formData.append(entry[0], value);
    })
    return Vue.axios.post<R>(apiPath, formData, {
      headers: { 'Content-Type': 'multipart/form-data' },
      onUploadProgress: function( progressEvent: any ) {
        const uploadPercentage = Math.round( ( progressEvent.loaded / progressEvent.total ) * 100 )
        onUploadProgress(uploadPercentage)
      }.bind(this)
    })
  },
  get<R>(apiPath: string, params: any = {}): Promise<AxiosResponse<R>> {
    console.log(apiPath, params)
    return Vue.axios.get<R>(apiPath, {params: params});
  },
  put<R>(apiPath: string, request: any): Promise<AxiosResponse<R>> {
    return Vue.axios.put<R>(apiPath, request);
  },
  delete<R>(apiPath: string): Promise<AxiosResponse<R>> {
    return Vue.axios.delete<R>(apiPath);
  },
}

export default ApiService
