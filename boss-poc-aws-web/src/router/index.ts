import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import ListMetric from '../views/ListMetric.vue'
import MetricData from '../views/MetricData.vue'

Vue.use(VueRouter)

  const routes: Array<RouteConfig> = [
  {
    path: '/',
    redirect: '/listMetric'
  },
  {
    path: '/listMetric',
    name: 'listMetric',
    component: ListMetric
  },
  {
    path: '/metric/:metricName',
    name: 'MetricData',
    component: MetricData
  }
]

const router = new VueRouter({
  base: process.env.BASE_URL,
  routes
})

export default router
