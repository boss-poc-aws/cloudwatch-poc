# boss-poc-aws-web

## In boss-poc-aws-web directory
```
cd boss-poc-aws-web
```

## Load dependencies
```
npm install
```

### Run local
```
npm run serve
```


# boss-poc-aws-api

## In boss-poc-aws-api directory
```
cd boss-poc-aws-api
```

## Run local
```
./gradlew bootRun
```