package boss.poc.aws.api.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider
import software.amazon.awssdk.regions.Region
import software.amazon.awssdk.services.cloudwatch.CloudWatchClient

@Configuration
class CloudWatchClient {
    @Bean
    fun build(awsConfig: AwsConfig): CloudWatchClient {
        val awsCredentials = AwsBasicCredentials.create(awsConfig.accessKey, awsConfig.secretKey)
        return CloudWatchClient.builder()
            .credentialsProvider(StaticCredentialsProvider.create(awsCredentials))
            .region(Region.of(awsConfig.region))
            .build()
    }
}