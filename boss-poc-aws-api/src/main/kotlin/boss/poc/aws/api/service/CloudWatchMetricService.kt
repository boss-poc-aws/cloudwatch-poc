package boss.poc.aws.api.service

import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import software.amazon.awssdk.services.cloudwatch.CloudWatchClient
import software.amazon.awssdk.services.cloudwatch.model.*
import java.time.Instant
import java.time.temporal.ChronoUnit

@Service
class CloudWatchMetricService(private val client: CloudWatchClient) {
    private val logger = LoggerFactory.getLogger(this::class.java)

    fun getMetricsByNamespace(namespace: String): List<String> {
        var done = false
        var nextToken: String? = null
        val results: HashSet<String> = HashSet()
        while (!done) {
            var response: ListMetricsResponse
            response = if (nextToken == null) {
                val request = ListMetricsRequest.builder()
                    .namespace(namespace)
                    .build()
                client.listMetrics(request)
            } else {
                val request = ListMetricsRequest.builder()
                    .namespace(namespace)
                    .nextToken(nextToken)
                    .build()
                client.listMetrics(request)
            }
            for (metric in response.metrics()) {
                logger.info("Retrieved metric {}", metric.metricName())
            }
            results.addAll(response.metrics().map { it.metricName() })
            if (response.nextToken() == null) {
                done = true
            } else {
                nextToken = response.nextToken()
            }
        }
        return results.toList()
    }

    fun getMetricData(namespace: String, metricName: String, stat: String): List<HashMap<String, Any>> {
        try { // Set the date
            val endDate = Instant.now()
            val start = endDate.minus(12, ChronoUnit.HOURS)
            val met =
                Metric.builder()
                    .metricName(metricName)
                    .namespace(namespace)
                    .build()
            val metStat =
                MetricStat.builder()
                    .stat(stat)
                    .period(6000)
                    .metric(met)
                    .build()
            val dataQuery =
                MetricDataQuery.builder()
                    .metricStat(metStat)
                    .id("results")
                    .returnData(true)
                    .build()
            val dq: MutableList<MetricDataQuery?> = arrayListOf(dataQuery)
            val getMetReq =
                GetMetricDataRequest.builder()
                    .maxDatapoints(100)
                    .startTime(start)
                    .endTime(endDate)
                    .metricDataQueries(dq)
                    .build()
            val response = client.getMetricData(getMetReq)

            val result = response.metricDataResults().firstOrNull()
            return result?.values()?.mapIndexed { index, value ->
                val timestamp = result.timestamps()[index]
                hashMapOf(
                    Pair("timestamp", timestamp),
                    Pair("value", value)
                )
            }?: listOf()
        } catch (e: CloudWatchException) {
            logger.error(e.awsErrorDetails().errorMessage())
            throw e
        }
    }
}