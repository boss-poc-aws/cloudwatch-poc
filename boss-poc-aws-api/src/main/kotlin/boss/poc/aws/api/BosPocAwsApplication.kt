package boss.poc.aws.api

import boss.poc.aws.api.config.AwsConfig
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication

@SpringBootApplication
@EnableConfigurationProperties(
	AwsConfig::class
)
class BosPocAwsApplication

fun main(args: Array<String>) {
	runApplication<BosPocAwsApplication>(*args)
}
