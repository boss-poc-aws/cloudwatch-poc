package boss.poc.aws.api.config

import org.springframework.boot.context.properties.ConfigurationProperties

@ConfigurationProperties("aws-config")
data class AwsConfig(
    var region: String = "",
    var accessKey: String = "",
    var secretKey: String = ""
)
