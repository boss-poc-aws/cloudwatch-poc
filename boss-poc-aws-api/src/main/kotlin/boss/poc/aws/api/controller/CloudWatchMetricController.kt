package boss.poc.aws.api.controller

import boss.poc.aws.api.service.CloudWatchMetricService
import org.springframework.web.bind.annotation.*
import software.amazon.awssdk.services.cloudwatch.model.Metric
import software.amazon.awssdk.services.cloudwatch.model.MetricDataResult

@CrossOrigin(origins = ["*"], maxAge = 3600)
@RestController
@RequestMapping("/cloudwatch/metric")
class CloudWatchMetricController(
    val cloudWatchMetricService: CloudWatchMetricService
) {

    //Example localhost:8181/cloudwatch/metric/list?namespace=EC2
    @GetMapping("/list")
    fun getMetrics(
        @RequestParam namespace: String
    ): List<String> {
        return cloudWatchMetricService.getMetricsByNamespace("AWS/$namespace")
    }

    //Example localhost:8181/cloudwatch/metric/data?namespace=EC2&metricName=DiskReadBytes&stat=Average
    @GetMapping("/data")
    fun getMetricData(
        @RequestParam(defaultValue = "") namespace: String,
        @RequestParam(defaultValue = "") metricName: String,
        @RequestParam(defaultValue = "") stat: String
    ): List<Map<String, Any>> {
        return cloudWatchMetricService.getMetricData("AWS/$namespace", metricName, stat)
    }
}